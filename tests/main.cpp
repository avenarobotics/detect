#include "gtest/gtest.h"
#include "mysql_connector.h"
#include "mysql_connector_structs.h"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>


class DetectTest : public ::testing::Test {

protected:
    MysqlConnector connector{"172.18.18.4", "3306", "avena_db", "avena", "avena"};

    virtual void SetUp() {
        //Clear database
        int pid, status;
        pid = fork();
        if (pid)
        {
            waitpid(pid, &status, 0);
        }
        else
        {
            const char executable[] = "/opt/avena/cli/clear_database"; //binary to run
            execl(executable, "clear_database", nullptr);
        }

        //Save test images to the database
        cv::Mat image1 = cv::imread("utils/0_sample.png");
        cv::Mat image2 = cv::imread("utils/0_sample.png");
        std::vector<uint8_t> serialized_img1;
        std::vector<uint8_t> serialized_img2;
        cv::imencode(".png", image1, serialized_img1);
        cv::imencode(".png", image2, serialized_img2);

        std::stringstream ss1;
        ss1.write(reinterpret_cast<const char *>(serialized_img1.data()), serialized_img1.size());
        std::shared_ptr<std::iostream> is1{std::make_shared<std::iostream>(ss1.rdbuf())};

        std::stringstream ss2;
        ss2.write(reinterpret_cast<const char *>(serialized_img2.data()), serialized_img2.size());
        std::shared_ptr<std::iostream> is2{std::make_shared<std::iostream>(ss2.rdbuf())};

        auto * test_scene = new scene_t {};
        test_scene->camera_1_rgb = {is1};
        test_scene->camera_2_rgb = {is2};
        connector.setScene(test_scene);
        delete test_scene;
    }

    virtual void TearDown() {
        //Clear database
        int pid, status;
        pid = fork();
        if (pid)
        {
            waitpid(pid, &status, 0);
        }
        else
        {
            const char executable[] = "/opt/avena/cli/clear_database"; //binary to run
            execl(executable, "clear_database", nullptr);
        }
    }

};

TEST_F(DetectTest, testConnection){
    //call detect on the CLI
    int pid, status;
    pid = fork();
    if (pid)
    {
        waitpid(pid, &status, 0);
    }
    else
    {
        const char executable[] = "/opt/avena/cli/detect"; //binary to run
        execl(executable, "detect", nullptr);
    }

    // check results
    std::vector<uint32_t> cam1_rows{connector.getRowsId("item_cam1")};
    std::vector<uint32_t> cam2_rows{connector.getRowsId("item_cam2")};
    ASSERT_GT(cam1_rows.size(), 0);
    ASSERT_GT(cam2_rows.size(), 0);

}