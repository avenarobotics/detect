#ifndef DATABASECPPVOL3_IODATABASE_H
#define DATABASECPPVOL3_IODATABASE_H

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include <vector>
#include <ostream>
#include "opencv2/opencv.hpp"
#include "mysql_connector.h"
#include "mysql_connector_structs.h"

struct ImageInfo
{
    explicit ImageInfo(int _cols = -1, int _rows = -1, int _type = CV_8U) :
            cols(_cols), rows(_rows), type(_type){};
    int cols, rows, type;
    size_t Size() {return CV_ELEM_SIZE(type) * rows * cols;}
    size_t RowSize(){ return CV_ELEM_SIZE(type) * cols; }
};

bool ImageSerialize(std::ostream &out, const cv::Mat& mat);

bool ImageDeserialize(std::istream& in, cv::Mat& mat);

void insertImages(cv::Mat& image1, cv::Mat& image2, const std::string& table, MysqlConnector* connection);

std::vector<char*> getImagesFromDB(const std::string &table, MysqlConnector *connection, std::vector<int>& imgSize, int& sceneId);

void deleteMasksFromDatabase(MysqlConnector* connection);

void saveDetectronResults(const std::string &table, MysqlConnector * connector,
                          std::vector<std::string>& instances, std::vector<std::string>& masks,
                          std::vector<float>& scores, int &sceneId,     std::map<std::string, int> &classMap,
                          float detection_threshold);
std::map<std::string, int> getClassMapping(MysqlConnector & connector);
#endif //DATABASECPPVOL3_IODATABASE_H
