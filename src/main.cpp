#include <boost/beast/core.hpp>
#include <utility>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include <opencv2/core.hpp>
#include "iodatabase.h"
#include <boost/asio/strand.hpp>
#include <memory>
#include <boost/program_options.hpp>

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace po = boost::program_options;

void fail(beast::error_code ec, char const *what) {
    std::cerr << what << ": " << ec.message() << "\n";
}

// Sends a WebSocket message and prints the response
class WebsocketSession : public std::enable_shared_from_this<WebsocketSession> {
    /***
     * Class responsible for creating a websocket connection to the detectron server.
     * The ip for the server is hardcoded to 172.18.18.3
     * The ip for the mysql database connection is hardcoded to 172.18.18.4
     * params resolver_, ws_,
     */
    tcp::resolver resolver_;
    websocket::stream<beast::tcp_stream> ws_;
    beast::flat_buffer buffer_;
    std::string host_;
    MysqlConnector *connection;
    std::map<std::string, int> classMap_;
    int camId_;
    const char *data_;
    int message_size_{};
    int sceneId_;
    float det_thresh_;

public:
    // Resolver and socket require an io_context
    WebsocketSession(net::io_context &ioc, MysqlConnector* connector, int & sceneId, int camId,     std::map<std::string,
                     int> &classMap, float det_thresh)
            : resolver_(net::make_strand(ioc)), ws_(net::make_strand(ioc)), connection(connector),
            sceneId_(sceneId), camId_(camId), classMap_(classMap), det_thresh_(det_thresh) {
        // set websocket payloiads type to binary
        this->ws_.binary(true);
    }

    void createResultVectors(nlohmann::json &jsonObj, std::vector<std::string> &masks, std::vector<std::string> &instances,
                        std::vector<float> &scores, std::vector<std::string> &class_ids, std::vector<uint8_t>& annotated_jpeg) {
        //get the masks
        for (auto &el : jsonObj["masks"].items()) {
            masks.push_back(el.value().dump());
        }
        for (auto &el : jsonObj["classes"].items()) {
            instances.push_back(el.value());
        }
        for (auto &el : jsonObj["scores"].items()) {
            scores.push_back(el.value());
        }
        for (auto &el : jsonObj["class_ids"].items()) {
            class_ids.push_back(el.value());
        }
        uint8_t value;
        for (auto &el : jsonObj["annotated_image"].items())
        {
            value = el.value()[0];
            annotated_jpeg.push_back(value);
        }
    }

    void saveDetectionResultToDB(std::string &json, int &rgbCamID) {
        nlohmann::json jsonObj;
        std::stringstream(json) >> jsonObj;
        std::vector<std::string> masks{};
        std::vector<std::string> instances{};
        std::vector<float> scores{};
        std::vector<std::string> class_ids{};
        std::vector<uint8_t> annotated_jpeg{};

        createResultVectors(jsonObj, masks, instances, scores, class_ids, annotated_jpeg);

        std::stringstream ss;
        ss.write(reinterpret_cast<const char*>(annotated_jpeg.data()), annotated_jpeg.size());

        std::vector<uint32_t> scene_ids = this->connection->getRowsId("scene");
        this->connection->writeBLOB("scene", "annotated_camera_"+std::to_string(rgbCamID), &ss, scene_ids[0]);
        
        std::string camString = "item_cam" + std::to_string(rgbCamID);
        saveDetectronResults(camString, this->connection, instances, masks, scores, this->sceneId_, this->classMap_, this->det_thresh_);
    }

    // Start the asynchronous operation
    void run(char const *host, char const *port, char const *data, int imgSize) {
        // Save these for lat
        host_ = host;
        data_ = data;
//        this->camId_ = camId;
        this->message_size_ = imgSize;
        // Look up the domain name
        resolver_.async_resolve(host, port, beast::bind_front_handler(&WebsocketSession::on_resolve, shared_from_this()));
    }

    void on_resolve(beast::error_code ec, tcp::resolver::results_type results) {
        if (ec)
            return fail(ec, "resolve");

        // Set the timeout for the operation
        beast::get_lowest_layer(ws_).expires_after(std::chrono::seconds(30));

        // Make the connection on the IP address we get from a lookup
        beast::get_lowest_layer(ws_).async_connect(results,
                                                   beast::bind_front_handler(&WebsocketSession::on_connect, shared_from_this()));
    }

    void on_connect(beast::error_code ec, tcp::resolver::results_type::endpoint_type ep) {
        if (ec)
            return fail(ec, "connect");

        // Turn off the timeout on the tcp_stream, because
        // the websocket stream has its own timeout system.
        beast::get_lowest_layer(ws_).expires_never();

        // Set suggested timeout settings for the websocket
        ws_.set_option(
                websocket::stream_base::timeout::suggested(
                        beast::role_type::client));

        // Set a decorator to change the User-Agent of the handshake
        ws_.set_option(websocket::stream_base::decorator(
                [](websocket::request_type &req) {
                    req.set(http::field::user_agent,
                            std::string(BOOST_BEAST_VERSION_STRING) +
                            " websocket-client-async");
                }));

        // Update the host_ string. This will provide the value of the
        // Host HTTP header during the WebSocket handshake.
        // See https://tools.ietf.org/html/rfc7230#section-5.4
        host_ += ':' + std::to_string(ep.port());

        // Perform the websocket handshake
        ws_.async_handshake(host_, "/",
                            beast::bind_front_handler(
                                    &WebsocketSession::on_handshake,
                                    shared_from_this()));
    }

    void on_handshake(beast::error_code ec) {
        if (ec)
            return fail(ec, "handshake");
        // Send the message
        ws_.async_write(net::buffer(data_, this->message_size_),
                        beast::bind_front_handler(&WebsocketSession::on_write, shared_from_this()));
    }

    void on_write(beast::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);
        if (ec)
            return fail(ec, "write");

        // Read a message into our buffer
        ws_.async_read(buffer_, beast::bind_front_handler(&WebsocketSession::on_read, shared_from_this()));
    }

    void on_read(beast::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);

        if (ec)
            return fail(ec, "read");
        std::string json{boost::beast::buffers_to_string(buffer_.data())};
        saveDetectionResultToDB(json, this->camId_);
        // Close the WebSocket connection
        ws_.async_close(websocket::close_code::normal,
                        beast::bind_front_handler(&WebsocketSession::on_close, shared_from_this()));
    }

    void on_close(beast::error_code ec) {
        if (ec)
            return fail(ec, "close");
    }

};

template <typename T, typename R,  typename ...Args>
double executeAndMeasureTime(T function, R& retValHolder, Args&&... args){
    auto t_start = std::chrono::high_resolution_clock::now();
    retValHolder = function(std::forward<Args>(args)...);
    auto t_stop = std::chrono::high_resolution_clock::now();
    return (std::chrono::duration_cast<std::chrono::nanoseconds>(t_stop - t_start).count()) * 1e-9;
}


// Sends a WebSocket message and prints the response
int main(int argc, char *argv[]) {
    try {
        float detection_threshold{0.7};
        po::options_description desc("Allowed options");
        desc.add_options()
                ("help",  "produce help message")
                ("dt", po::value<float>(), "sets a threshold for detection");
        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("dt")) {
            detection_threshold = vm["dt"].as<float>();
            printf("%s%.2f%s", "Detection accuracy threshold set to ", detection_threshold * 100, "%\n");
        }

        auto const serverIpAddress = "172.18.18.3";
        auto const port = "8765";

        MysqlConnector connector{"172.18.18.4", "3306", "avena_db", "avena", "avena"};
        net::io_context ioc;

//        initialDbOps(&connector);

        std::vector<int> img_size(2);
        int sceneId{};
        auto t_start = std::chrono::high_resolution_clock::now();
        std::vector<char*> img = getImagesFromDB("scene", &connector, img_size, sceneId);
        auto t_stop = std::chrono::high_resolution_clock::now();
        int cameraId = 1;
        printf("%s%f%s", "Getting images from database took: ", (std::chrono::duration_cast<std::chrono::nanoseconds>(t_stop - t_start).count()) * 1e-9, "seconds\n");

        printf("Sending images to detectron server and saving results to database...\n");
        t_start = std::chrono::high_resolution_clock::now();

        //deleting existing masks from database
        deleteMasksFromDatabase(&connector);
        std::map<std::string, int> classMap{getClassMapping(connector)};

        // Launch the asynchronous operation
        for (int i = 0; i < img.size(); i++){
        std::make_shared<WebsocketSession>(ioc, &connector, sceneId, cameraId, classMap, detection_threshold)->run(serverIpAddress, port, img[i], img_size[i]);
        cameraId++;
        }
        // Run the I/O service. The call will return when
        // the socket is closed.
        ioc.run();
        t_stop = std::chrono::high_resolution_clock::now();
        printf("%s%f%s", "Sending and saving took: ", (std::chrono::duration_cast<std::chrono::nanoseconds>(t_stop - t_start).count()) * 1e-9, "seconds\n");

    }
    catch (std::exception const &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
