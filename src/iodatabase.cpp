#include "iodatabase.h"

bool ImageSerialize(std::ostream &out, const cv::Mat &mat) {
    ImageInfo mi(mat.cols, mat.rows, mat.type());
    std::ostream::sentry s(out);
    if (!(s && out.good()))
        return false;
    out.write((char *) (&mi), sizeof(ImageInfo));
    if (mat.isContinuous())
        out.write((char *) mat.data, mi.Size());
    else {
        size_t rowsz = mi.RowSize();
        for (int r = 0; r < mi.rows; ++r)
            out.write((char *) mat.ptr<char>(r), rowsz);
    }
    out.flush();
    return out.good();
}

std::stringstream &operator<<(std::stringstream &out, const cv::Mat &mat) {
    ImageSerialize(out, mat);
    return out;
}

bool ImageDeserialize(std::istream &in, cv::Mat &mat) {
    std::istream::sentry s(in);
    if (!(s && in.good())) {
        mat = cv::Mat();
        return false;
    }
    ImageInfo mi;
    in.read((char *) (&mi), sizeof(ImageInfo));
    if (mi.Size() < 0) {
        mat = cv::Mat();
        return false;
    }
    mat = cv::Mat(mi.rows, mi.cols, mi.type);
    in.read((char *) mat.data, mi.Size());
    return in.good();
}

std::istream &operator>>(std::istream &in, cv::Mat &mat) {
    ImageDeserialize(in, mat);
    return in;
}

//void insertImages(cv::Mat &image1, cv::Mat &image2, const std::string &table, MysqlConnector * connector) {
//    std::vector<uint8_t> serialized_img1;
//    std::vector<uint8_t> serialized_img2;
//    cv::imencode(".png", image1, serialized_img1);
//    cv::imencode(".png", image2, serialized_img2);
//
//    std::stringstream ss1;
//    ss1.write(reinterpret_cast<const char *>(serialized_img1.data()), serialized_img1.size());
//    std::shared_ptr<std::iostream> is1{std::make_shared<std::iostream>(ss1.rdbuf())};
////    std::istream is1(ss1.rdbuf());
//
//    std::stringstream ss2;
//    ss2.write(reinterpret_cast<const char *>(serialized_img2.data()), serialized_img2.size());
//    std::shared_ptr<std::iostream> is2{std::make_shared<std::iostream>(ss2.rdbuf())};
//
////    std::istream is2(ss2.rdbuf());
//
//    try {
//        int row_id = connector->writeBLOB("scene", "camera_1_rgb", &is1);
//        connector->writeBLOB("scene", "camera_2_rgb", &is2, row_id);
//
//    } catch (sql::SQLException &e) {
//        std::cout << "# ERR: SQLException in " << __FILE__;
//        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
//        std::cout << "# ERR: " << e.what();
//        std::cout << " (MySQL error code: " << e.getErrorCode();
//        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
//    }
//}
void deleteMasksFromDatabase(MysqlConnector * connector){
    try{
        connector->clearTable("item_cam1");
        connector->clearTable("item_cam2");

    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }


}
void saveDetectronResults(const std::string &table, MysqlConnector* connector,
                          std::vector<std::string> &instances, std::vector<std::string> &masks,
                          std::vector<float> &scores, int &sceneId, std::map<std::string, int> &classMap, float detection_threshold) {
    try {
        printf(" %s%ld%s%ld%s%ld%s%s%s","Size of masks, scores, instances: ", masks.size()," ", scores.size(), " ", instances.size(), " for table ",table.c_str(), "\n");
        assert(instances.size() == masks.size() && masks.size() == scores.size());
        if (masks.empty()){
            printf("%s", "No objects present in the scene or no detections were made\n");
            return;
        }
        int saved_count{0};
        for (int i = 0; i < masks.size(); i++){
            if (scores[i] > detection_threshold){
                uint32_t rowId = connector->writeFK(table, "scene_id", sceneId);
                connector->writeString(table, "mask", masks[i], rowId);
                connector->writeInt(table, "label_id", classMap[instances[i]], rowId);
                connector->writeDouble(table, "accuracy", scores[i], rowId);
                saved_count++;
            }
        }
        printf("%s%i%s%zu%s", "Saved data for ", saved_count," objects that match the detection threshold out of total ",
               instances.size(),  " detections made\n");

    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }

}

std::map<std::string, int> getClassMapping(MysqlConnector & connector) {

    std::map<std::string, int> classMap{};
    label_t label{};
    try {
        std::vector<uint32_t> labelTableIds{connector.getRowsId("label")};
        for (auto &el: labelTableIds){
            connector.getLabel(&label, el);
            classMap[label.label] = label.label_id;
        }

    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }

    printf("Loaded class mappings...\n");

    return classMap;
}

std::vector<char*> getImagesFromDB(const std::string &table, MysqlConnector * connector, std::vector<int> &imgSize, int &sceneID) {

    scene_t sceneData{};

    try {

        std::vector<uint32_t>imagesIds{connector->getRowsId("scene")};
        auto max = *max_element(imagesIds.begin(), imagesIds.end());; // c++11
        connector->getScene(&sceneData, max);
        sceneID = max;

    } catch (sql::SQLException &e) {
        std::cout << "# ERR: SQLException in " << __FILE__;
        std::cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << std::endl;
        std::cout << "# ERR: " << e.what();
        std::cout << " (MySQL error code: " << e.getErrorCode();
        std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
    }
    std::shared_ptr<std::iostream> blb_cam1{sceneData.camera_1_rgb};
    std::shared_ptr<std::iostream> blb_cam2{sceneData.camera_2_rgb};
    std::string str_cam1;
    std::string str_cam2;
    std::string str;

    std::istreambuf_iterator<char> isb_cam1{std::istreambuf_iterator<char>(*blb_cam1)};
    std::istreambuf_iterator<char> isb_cam2{std::istreambuf_iterator<char>(*blb_cam2)};
    str_cam1 = std::string(isb_cam1, std::istreambuf_iterator<char>());
    str_cam2 = std::string(isb_cam2, std::istreambuf_iterator<char>());

    char *buffer_cam1{nullptr};
    char *buffer_cam2{nullptr};
    std::vector<char*> buffers{buffer_cam1, buffer_cam2};
    std::vector<std::shared_ptr<std::iostream>> cam_blbs{blb_cam1, blb_cam2};
    for (int i = 0; i < cam_blbs.size(); i++){
        // get length of file:
        cam_blbs[i]->seekg(0, std::istream::end);
        int length = cam_blbs[i]->tellg();
        imgSize[i] = length;
        cam_blbs[i]->seekg(0, std::istream::beg);
        buffers[i] = new char[length];
        // read data as a block:
        cam_blbs[i]->read(buffers[i], length);
        std::string opsRes =  "Read " + std::to_string(cam_blbs[i]->gcount()) + " of " + std::to_string(length) + " bytes\n";
        printf("%s", opsRes.c_str());
    }
    return buffers;

}
